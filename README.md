# This repository was moved!

New location: [https://github.com/galaxias-ufsc/pycasso2/](https://github.com/galaxias-ufsc/pycasso2/)

This software is supported by Fundação de Amparo à Pesquisa e Inovação de Santa Catarina (FAPESC) and Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq).
